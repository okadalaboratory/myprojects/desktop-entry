# デスクトップにショートカットアイコンを作成する方法

## Desktop Entryファイルを作成する
以下のようなファイルをデスクトップの置く<br>
ファイル名は [任意のファイル名].desktop

```
cd ~/Desktop
vi myApp.desktop

[Desktop Entry]
Name=MyApp
Comment=Application
Exec=/home/roboworks/myApp.sh
Icon=/home/roboworks/icon.png
Terminal=false
Type=Application
```
myApps.sh は
```
#!/bin/bash
# 端末（terminator）を起動し、sshで接続する
# terminator -x sshpass -p [バスワード] ssh [ユーザID]@[接続先PC]
terminator -x sshpass -p password ssh administrator@hsrc.local
```

## 試してみる
ショートカットをダブルクリックすると、terminatorが起動し、sshでリモート端末に接続する。
```
$ cd ~
$ git clone https://gitlab.com/okadalaboratory/myprojects/desktop-entry.git
$ cp desktop-entry/dgx.desktop ~/Desktop
$ cp desktop-entry/hsrc.desktop ~/Desktop
```

デスクトップにショートカットが表示されるので、右クリックから「起動を許可する」を実行する

アイコンが表示されるので、ダブルクリックすると実行される。

# Ubuntu20.04でデスクトップのアイコンを大きくする
```
sudo apt install gnome-tweaks
```
```
gnome-tweaks
```
拡張機能 -> Desktop icons をオンに -> 歯車アイコン　　-> 「デスクトップのアイコンサイズ」を「大きい」に変更する


# キーボードショートカットを設定する
↑で作成したシェルスクリプトはキーボードショートカットに登録することもできる。

設定→キーボードショートカット　<br>
一番下までスクロールすると、「＋」ボタンで独自のキーボードショートカットを登録できる。

- 名前：DXGstation
- コマンド://home/roboworks/desktop-entry/dgx.sh
- ショートカット：Ctl+Alt+N

Ctl＋Alt＋N を同時に押すと、terminatorが起動し、sshでリモート端末に接続する。
